public class Exercise1{
	
	/*Overloading*/
	
	// tao ham tinh phep nhan voi 2 so nguyen
	public int multiplication(int a, int b){
		return a*b;
	}
	
	// tao ham tinh phep nhan voi 3 so nguyen co cung ten voi ham tren
	public int multiplication(int a, int b, int c){
		return a*b*c;
	}
	
	// tai ham tinh phep nhan voi 2 so double
	public double multiplication(double a, double b){
		return a*b;
	}
	
	public static void main(String[] args){
		Exercise1 ex1 = new Exercise1();
		
		System.out.println(ex1.multiplication(2,3)); // in ra tich 2 so nguyen khi truyen vao ham 2 so nguyen
		System.out.println(ex1.multiplication(2,3,4)); // in ra tich 3 so nguyen khi truyen vao ham 3 so nguyen
		System.out.println(ex1.multiplication(2, 3.0)); // in ra tich 2 so nguyen khi truyen vao ham 2 so kieu double
	}
}