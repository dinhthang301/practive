import java.util.*;

/* Static Binding and Dynamic Binding*/

class Thailan{
	public void area(){
		System.out.println("Dong Nam A");
	}
	
	public static void speak(){
		System.out.println("Tieng Thai");
	}
}


public class Exercise4 extends Thailan {
	public void area(){
		System.out.println("Bac My");
	}
	
	public static void speak(){
		System.out.println("Tieng viet");
	}
	
	public static void main(String[] args){
		Thailan thai1 = new Exercise4();
		Thailan thai2 = new Thailan();
		
		// Static binding
		System.out.printf("Nguoi Thai 1 noi tieng ");
		thai1.speak();
		System.out.printf("Nguoi Thai 2 noi tieng ");
		thai2.speak();
		
		// Dynamic binding
		System.out.printf("Nguoi Thai 1 o ");
		thai1.area();
		System.out.printf("Nguoi Thai 2 o ");
		thai2.area();
	}
}