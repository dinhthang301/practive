import java.util.*;

/*Final virable, method and class*/

class Parent{ // Class cha
	
	int num = 100;
	
	final void dislay(){
		System.out.println("This is Parent");
	}
}

class Branch extends Parent{ // Class nhánh
	/*
	// chuong trinh se bao loi do khong the override duoc khi co thuoc tinh Final
	@Override 
	void dislay(){
	System.out.println("This is Branch");
	}
	*/
}

public class Exercise3{
	
	static void show(){
		System.out.println("Method duoc su dung de in dong nay");
	}
	
	public static void main(String[] args){
		
		final Parent com1 = new Parent(); // final kieeu bien khong nguyen thuy thi cacs doi tuwowng ben trong van co the thay doi
		com1.num = 10;
		
		System.out.println(com1.num);
		
		show();
	}
}