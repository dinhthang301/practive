package exercise10;

public class FirstClass{
	private String name = "Dinh Thang";
	private int age = 23;
	private String department = "LG DCV_Member";
	
	protected FirstClass(String name, int age){
		this.name = name;
		this.age = age;
	}
	
	public FirstClass(){
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public void setAge(int age){
		this.age = age;
	}		
	
	public void setDepartment(String department){
		this.department = department;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getAge(){
		return this.age;
	}
	
	public String getDepartment(){
		return this.department;
	}
}