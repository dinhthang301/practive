import java.util.*;

class Parent{
	int number = 100;
}

class LGlaptop extends Parent{
	int number;
	String quality;
	
	
	// khoi tao contructor
	public LGlaptop(int number, String quality){
		this.number = number; 
		this.quality = quality;
	}
	
	// ham hien thi
	void dislay(){
		System.out.println(number);
		System.out.println(quality);
		System.out.println(super.number); // hien thi gia tri cuar Parent
	}

}


public class Exercise7{
	public static void main(String[] args){
		LGlaptop check = new LGlaptop(50, "Good");
		check.dislay();
	}
}