import java.util.*;

interface A{
	void check1();
	void check2();
	void check3();
	void check4();
}

abstract class B implements A{
	public void check3(){
		System.out.println("is check3");
	}
	
	public void check4(){
		System.out.println("is check4");
	}
}

class C extends B{
	public void check1(){
		System.out.println("is check1");
	}
	
	public void check2(){
		System.out.println("is check2");
	}
}

public class Exercise6{
	public static void main(String[] args){
		A test = new C();
		test.check1();
		test.check2();
		test.check3();
		test.check4();
	}
}