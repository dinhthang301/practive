import java.util.*;

public class Exercise9{
	
	static int fibonaci(int n) { 
    if (n <= 1) 
       return n; 
    return fibonaci(n-1) + fibonaci(n-2); 
    } 
	public static void main(String[] args){
	Scanner scan = new Scanner(System.in);
    int n = scan.nextInt(); 
	
	for(int i=1; i<=n; i++){
    System.out.printf(fibonaci(i) + " "); 
	}
	
	}
}