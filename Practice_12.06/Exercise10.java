import exercise10.FirstClass;
import exercise10.SecondClass;
import java.util.*;

public class Exercise10{
	
	public static void main(String[] args){
		
		Scanner scan = new Scanner(System.in);
		
		String name = scan.nextLine();
		int age = scan.nextInt();
		scan.close();
		
		//FirstClass test = new FirstClass(name,age); khong the khoi tao duoc do contructor dang de protected
		
		FirstClass test = new FirstClass();
		
		System.out.println(test.getName());
		System.out.println(test.getAge());
		System.out.println(test.getDepartment());
		
		
		SecondClass test2 = new SecondClass();
		test2.show(name, age);
	}
}