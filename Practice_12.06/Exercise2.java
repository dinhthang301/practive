import java.util.*;

/*Overriding*/

class Parent{ // Class cha
	void dislay(){
		System.out.println("This is Parent");
	}
}

class Branch extends Parent{ // Class nhánh
	@Override // Override lại hàm dislay của class cha
	void dislay(){
	System.out.println("This is Branch");
	}
}

public class Exercise2{
	
	public static void main(String[] args){
		Parent com1 = new Parent();
		Parent com2 = new Branch();
		
		com1.dislay();

		com2.dislay();
	}
	
}