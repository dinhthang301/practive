import java.util.*;

/*downcasting and upcasting*/

class Parent{
	void dislay(){
		System.out.println("LG DCV.........");
	}
	
	void department(){
		System.out.println("VT.........");
	}
}

class Branch extends Parent{
	
	@Override
	void department(){
		System.out.println("SD.........");
	}
	
	void programing(){
		System.out.println("Java.........");
	}
}

public class Exercise5{
	public static void main(String[] args){
		
		//DOWNCASTING
	
	Parent event1 = new Branch();
	event1.department();
	//event.programing(); ham nay se khong goi duoc do event1 co kieu Parent
	if (event1 instanceof Branch){
		Branch downcasting = (Branch) event1; // downcasting
		downcasting.programing(); // co the goi ham programing khi da downcasting
	}
	
	
		// UPCASTING
		
	Branch event2 = new Branch();
	event2.programing();
	event2.department();
	Parent upcasting = (Parent) event2;
	//upcasting.programing(); khong the su dung ham nay khi da upcasting len parent
	upcasting.dislay();
	upcasting.department();
	
	}
}